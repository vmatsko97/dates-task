# ruby 2.5.0

require 'date'

class Event
  attr_reader :start, :end

  def initialize(start_time, end_time)
    @start = start_time
    @end = end_time
  end

  # This method overrides equality operator.
  # It was needed ONLY in tests, so I could compare real and expected results
  # Basically you can remove it
  def ==(other)
    self.start == other.start && self.end == other.end
  end
end

# The actual overlapping class
# See `/spec/app_test` for usage examples
class Overlap
  attr_reader :already_booked, :available

  def initialize(already_booked, available)
    @already_booked = already_booked
    @available = available
  end

  def call
    result = []

    available_hash

    booked_hash

    available_hash.each do |k, array|
      booked_for_this_day = booked_hash.fetch(k, [])

      array.each do |timeslot|
        fits = booked_for_this_day.none? { |ev| overlap?(ev, timeslot[:q]) }

        result << timeslot[:real] if fits
      end
    end

    result
  end

  private

  def merge_ranges(ranges)
    ranges = ranges.sort_by(&:first)

    *outages = ranges.shift

    ranges.each do |range|
      lastr = outages[-1]

      if lastr.last >= range.first - 1e-100
        outages[-1] = lastr.first...[range.last, lastr.last].max
      else
        outages.push
      end
    end

    outages
  end

  def booked_hash
    @booked_hash ||= already_booked.group_by(&method(:wday)).tap do |hash|
      hash.each do |k, v|
        input_ranges = v.map { |event| q_time(event) }
        hash[k] = merge_ranges(input_ranges)
      end
    end
  end

  def formula
    @formula ||= '%L-%S-%M-%H'
  end

  def q_time(event)
    s_temp = event.start.strftime(formula).split('-').map(&:to_f)
    e_temp = event.end.strftime(formula).split('-').map(&:to_f)
    s = calculate(s_temp)
    e = calculate(e_temp)

    (s...e)
  end

  def calculate(array)
    array[0] + array[1] * 1000.0 + array[2] * 60 * 1000.0 + array[3] * 1000.0 * 60 * 60
  end

  def available_hash
    @available_hash ||= available.group_by(&method(:wday)).tap do |hash|
      hash.each do |k, v|
        hash[k] = v.map { |event| { q: q_time(event), real: event } }
      end
    end
  end

  def wday(event)
    event.start.wday
  end

  def overlap?(date_range_1, date_range_2)
    date_range_1.cover?(date_range_2.first) || date_range_2.cover?(date_range_1.first)
  end
end
