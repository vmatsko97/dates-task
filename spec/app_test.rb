require './app.rb'
require 'pry'

describe Overlap do
  subject { described_class.new(already_booked, available).call }

  let(:already_booked) { [] }
  let(:available) { [] }

  def build_event(start_string, end_string)
    start_date = DateTime.parse(start_string)
    end_date = DateTime.parse(end_string)

    Event.new(start_date, end_date)
  end

  it 'returns empty array on empty input' do
    expect(subject).to eq([])
  end

  context 'DEMO CASE #1' do
    let(:already_booked) do
      [
        build_event('2018-12-19 16-00-00', '2018-12-19 17:00:00'),
        build_event('2018-12-20 9:00:00', '2018-12-20 10:00:00')
      ]
    end

    let(:available) do
      [
        build_event('2018-12-19 16:00:00', '2018-12-19 17:00:00'),
        build_event('2018-12-20 9:30:00', '2018-12-20 11:30:00'),
        build_event('2018-12-21 9:00:00', '2018-12-21 11:00:00')
      ]
    end

    let(:expected_result) do
      [
        build_event('2018-12-21 9:00:00', '2018-12-21 11:00:00')
      ]
    end

    it 'returns correct result for demo test case 1' do
      expect(subject).to eq(expected_result)
    end
  end


  context 'DEMO CASE #2' do
    let(:already_booked) do
      [
        build_event('2018-12-19 16:00:00', '2018-12-19 17:00:00'),
        build_event('2018-12-20 9:00:00', '2018-12-20 10:00:00'),
        build_event('2018-12-21 13:00:00', '2018-12-21 13:30:00')
      ]
    end

    let(:available) do
      [
        build_event('2018-12-19 16:00:00', '2018-12-19 17:00:00'),
        build_event('2018-12-20 9:30:00', '2018-12-20 11:30:00'),
        build_event('2018-12-28 13:00:00', '2018-12-28 15:00:00'),
        build_event('2018-12-29 13:00:00', '2018-12-29 14:00:00')
      ]
    end

    let(:expected_result) do
      [
        build_event('2018-12-29 13:00:00', '2018-12-29 14:00:00')
      ]
    end

    it 'returns correct result for demo test case 2' do
      expect(subject).to eq(expected_result)
    end
  end

  context 'with basic overlapping' do
    context 'when one event starts before another and ends within it' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 14:00:00', '2018-12-19 15:30:00') ] }
      let(:expected_result) { [] }

      it 'they count as overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event starts within another and ends within it' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 15:10:00', '2018-12-19 15:30:00') ] }
      let(:expected_result) { [] }

      it 'they count as overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event starts within another and ends after it' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 15:30:00', '2018-12-19 16:30:00') ] }
      let(:expected_result) { [] }

      it 'they count as overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event starts within another and ends after it' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 15:30:00', '2018-12-19 16:30:00') ] }
      let(:expected_result) { [] }

      it 'they count as overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event starts right when another and ends right when another' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:expected_result) { [] }

      it 'they count as overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event starts before another and ends before it' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 13:30:00', '2018-12-19 14:30:00') ] }
      let(:expected_result) { available }

      it 'they count as NON-overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event starts after another and ends after it' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 20:30:00', '2018-12-19 21:30:00') ] }
      let(:expected_result) { available }

      it 'they count as NON-overlapping' do
        expect(subject).to eq(expected_result)
      end
    end
  end

  context 'takes week days into account' do
    context 'when two event have the same HOURS, but different WEEK DAYS' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-30 15:00:00', '2018-12-30 16:00:00') ] }
      let(:expected_result) { available }

      it 'they count as NON-overlapping' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when two event have the same HOURS and the same WEEK DAY' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-26 15:00:00', '2018-12-26 16:00:00') ] }
      let(:expected_result) { [] }

      it 'they count as overlapping' do
        expect(subject).to eq(expected_result)
      end
    end
  end

  context 'EDGE CASES' do
    context 'when one event starts right after another' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 16:00:00', '2018-12-19 17:00:00') ] }
      let(:expected_result) { available }

      it 'they count as NON-overlapping (discussed in chat)' do
        expect(subject).to eq(expected_result)
      end
    end

    context 'when one event end right before another' do
      let(:already_booked) { [ build_event('2018-12-19 15:00:00', '2018-12-19 16:00:00') ] }
      let(:available) { [ build_event('2018-12-19 14:00:00', '2018-12-19 15:00:00') ] }
      let(:expected_result) { available }

      it 'they count as NON-overlapping (discussed in chat)' do
        expect(subject).to eq(expected_result)
      end
    end
  end

  context 'PERFORMANCE TESTING' do
    context 'for totally random dates ' do
      let(:available) do
        100_000.times.map do
          random_start = DateTime.now - (rand * 100)
          random_end = random_start + 5

          Event.new(random_start, random_end)
        end
      end

      let(:already_booked) do
        100_000.times.map do
          random_start = DateTime.now - (rand * 100)
          random_end = random_start + 5

          Event.new(random_start, random_end)
        end
      end

      it 'finishes within 3 seconds' do
        start = Time.now

        subject

        finish = Time.now

        expect(finish - start).to be < 3
      end
    end
  end
end